from typing import Annotated

import typer

from volume_sync.autocomplete import complete_host_and_volume
from volume_sync.sync import Sync

app = typer.Typer()


@app.command()
def main(
    source: Annotated[
        str, typer.Argument(..., autocompletion=complete_host_and_volume)
    ],
    target: Annotated[
        str, typer.Argument(..., autocompletion=complete_host_and_volume)
    ],
):
    with Sync(source, target) as sync:
        sync.rsync_volumes()
