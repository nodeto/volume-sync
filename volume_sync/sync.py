from __future__ import annotations

import socket
from dataclasses import dataclass
from pathlib import Path
from typing import TypedDict

import paramiko

from volume_sync.autocomplete import TailscaleData


@dataclass
class DockerVolume:
    host: str
    volume: str
    user: str = "root"


class Sync:
    def __init__(self, source: str, dest: str):
        self.source_volume = self.parse_scp_address(source)
        self.dest_volume = self.parse_scp_address(dest, self.source_volume)
        # Initialize SSH client
        self.source = paramiko.SSHClient()
        self.source.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.dest = paramiko.SSHClient()
        self.dest.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def connect(self):
        self.source.connect(
            hostname=self.source_volume.host,
            username=self.source_volume.user,
        )
        self.dest.connect(
            hostname=self.dest_volume.host, username=self.dest_volume.user
        )

    def __enter__(self) -> "Sync":
        """
        Connect to both source and destination SSH servers.

        Returns:
            self: The current instance of Sync.
        """
        # Connect to source SSH server
        self.connect()

        return self

    def close(self) -> None:
        """
        Close both source and destination SSH connections.
        """
        # Close source SSH connection
        self.source.close()

        # Close destination SSH connection
        self.dest.close()

    def __exit__(self, *_) -> None:
        """
        Close both source and destination SSH connections.
        """
        self.close()

    def parse_scp_address(
        self, address: str, source_volume: DockerVolume | None = None
    ):
        """Parses an SCP-like address into username and host parts."""
        user_host, volume = address.split("/")
        if "@" in user_host:
            user, host = user_host.split("@")
        else:
            user = "root"
            host = user_host

        if not volume and source_volume is not None:
            volume = source_volume.volume

        return DockerVolume(user=user, host=host, volume=volume)

    def rsync_volumes(
        self,
    ):
        """
        Uses rsync to directly sync a Docker volume from a source host to a destination host.
        """
        try:
            print(f"Copy to {self.dest_volume.volume}")
            self.load_images()
            # Start rsync servers
            self.start_rsync_server()
            # Execute rsync to sync from the source to the destination
            rsync_cmd = (
                f"docker run --rm -v {self.source_volume.volume}:/data rsync-client "
                "-avz --compress-level 1 --delete /data/ "
                f"rsync://{self.dest_volume.user}@{self.dest_volume.host}/data/"
            )
            _, stdout, _ = self.source.exec_command(rsync_cmd)
            while line := stdout.readline():
                print(line, end="", flush=True)

        finally:
            self.stop_rsync_servers()

    def run_command(self, connection: paramiko.SSHClient, command: str):
        """Return the stdout of the given command."""
        _, stdout, stderr = connection.exec_command(command)
        return_code = stdout.channel.recv_exit_status()
        if return_code != 0:
            raise Exception(
                (stderr.read().decode("utf-8"), stdout.read().decode("utf-8"))
            )
        return stdout.read().decode("utf-8")

    def start_rsync_server(self):
        # Get tailscale IP address
        tailscale_ip = socket.gethostbyname(self.dest_volume.host)

        # Run rsync server
        self.run_command(
            self.dest,
            f"docker run --rm -dit -p {tailscale_ip}:873:873 -v {self.dest_volume.volume}:/data --name rsync-server rsync-server",
        )

    def load_images(self):
        # Ensure the latest image is present
        for connection, source_image_file in (
            (self.source, "rsync-client.tar.gz"),
            (self.dest, "rsync-server.tar.gz"),
        ):
            source_image = (
                Path(__file__).parent.joinpath(source_image_file).read_bytes()
            )
            stdin, stdout, _ = connection.exec_command(
                "gunzip --to-stdout | docker load"
            )

            # Write data to stdin
            stdin.write(source_image)
            stdin.flush()
            stdin.channel.shutdown_write()

            print(stdout.read().decode("utf-8"), end="", flush=True)

    def stop_rsync_servers(self):
        self.run_command(self.dest, "docker stop rsync-server")
