import re

from paramiko import SSHClient


def get_tailscale_ip(ssh_client: SSHClient) -> str:
    """
    Fetch the IPv4 address of the tailscale0 interface.

    Returns:
        str: The IPv4 address of tailscale0 or an empty string if not found.

    >>> get_tailscale_ip()
    '100.108.179.13'
    """

    # Run the command and get output
    cmd_output = (
        ssh_client.exec_command("ip address show dev tailscale0")[1].read().decode()
    )

    # Use regular expression to find the IPv4 address
    match = re.search(r"inet (\d+\.\d+\.\d+\.\d+)/", cmd_output)

    if match is None:
        raise Exception("Did not find tailscale ip.")

    return match.group(1)
