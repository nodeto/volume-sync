from dataclasses import dataclass
from typing import Any, Type, TypeVar, cast

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class DockerVolume:
    availability: str
    driver: str
    group: str
    labels: str
    links: str
    mountpoint: str
    name: str
    scope: str
    size: str
    status: str

    @staticmethod
    def from_dict(obj: Any) -> "DockerVolume":
        assert isinstance(obj, dict)
        availability = from_str(obj.get("Availability"))
        driver = from_str(obj.get("Driver"))
        group = from_str(obj.get("Group"))
        labels = from_str(obj.get("Labels"))
        links = from_str(obj.get("Links"))
        mountpoint = from_str(obj.get("Mountpoint"))
        name = from_str(obj.get("Name"))
        scope = from_str(obj.get("Scope"))
        size = from_str(obj.get("Size"))
        status = from_str(obj.get("Status"))
        return DockerVolume(
            availability,
            driver,
            group,
            labels,
            links,
            mountpoint,
            name,
            scope,
            size,
            status,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["Availability"] = from_str(self.availability)
        result["Driver"] = from_str(self.driver)
        result["Group"] = from_str(self.group)
        result["Labels"] = from_str(self.labels)
        result["Links"] = from_str(self.links)
        result["Mountpoint"] = from_str(self.mountpoint)
        result["Name"] = from_str(self.name)
        result["Scope"] = from_str(self.scope)
        result["Size"] = from_str(self.size)
        result["Status"] = from_str(self.status)
        return result


def docker_volume_from_dict(s: Any) -> DockerVolume:
    return DockerVolume.from_dict(s)


def docker_volume_to_dict(x: DockerVolume) -> Any:
    return to_class(DockerVolume, x)
