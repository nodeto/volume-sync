from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Any, Callable, Dict, List, Optional, Type, TypeVar, cast

import dateutil.parser

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return {k: f(v) for (k, v) in x.items()}


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class CurrentTailnet:
    name: str
    magic_dns_suffix: str
    magic_dns_enabled: bool

    @staticmethod
    def from_dict(obj: Any) -> "CurrentTailnet":
        assert isinstance(obj, dict)
        name = from_str(obj.get("Name"))
        magic_dns_suffix = from_str(obj.get("MagicDNSSuffix"))
        magic_dns_enabled = from_bool(obj.get("MagicDNSEnabled"))
        return CurrentTailnet(name, magic_dns_suffix, magic_dns_enabled)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Name"] = from_str(self.name)
        result["MagicDNSSuffix"] = from_str(self.magic_dns_suffix)
        result["MagicDNSEnabled"] = from_bool(self.magic_dns_enabled)
        return result


class OS(Enum):
    I_OS = "iOS"
    LINUX = "linux"
    MAC_OS = "macOS"


class Relay(Enum):
    ORD = "ord"
    TOR = "tor"


class Tag(Enum):
    TAG_RUNDECK = "tag:rundeck"
    TAG_SERVER = "tag:server"


@dataclass
class Self:
    id: str
    public_key: str
    host_name: str
    dns_name: str
    os: OS
    user_id: float
    tailscale_ips: List[str]
    cur_addr: str
    relay: Relay
    rx_bytes: int
    tx_bytes: int
    created: datetime
    last_write: datetime
    last_seen: datetime
    last_handshake: datetime
    online: bool
    exit_node: bool
    exit_node_option: bool
    active: bool
    peer_apiurl: List[str]
    in_network_map: bool
    in_magic_sock: bool
    in_engine: bool
    tags: Optional[List[Tag]] = None
    addrs: Optional[List[str]] = None
    ssh_host_keys: Optional[List[str]] = None
    primary_routes: Optional[List[str]] = None
    key_expiry: Optional[datetime] = None
    capabilities: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> "Self":
        assert isinstance(obj, dict)
        id = from_str(obj.get("ID"))
        public_key = from_str(obj.get("PublicKey"))
        host_name = from_str(obj.get("HostName"))
        dns_name = from_str(obj.get("DNSName"))
        os = OS(obj.get("OS"))
        user_id = from_float(obj.get("UserID"))
        tailscale_ips = from_list(from_str, obj.get("TailscaleIPs"))
        cur_addr = from_str(obj.get("CurAddr"))
        relay = Relay(obj.get("Relay"))
        rx_bytes = from_int(obj.get("RxBytes"))
        tx_bytes = from_int(obj.get("TxBytes"))
        created = from_datetime(obj.get("Created"))
        last_write = from_datetime(obj.get("LastWrite"))
        last_seen = from_datetime(obj.get("LastSeen"))
        last_handshake = from_datetime(obj.get("LastHandshake"))
        online = from_bool(obj.get("Online"))
        exit_node = from_bool(obj.get("ExitNode"))
        exit_node_option = from_bool(obj.get("ExitNodeOption"))
        active = from_bool(obj.get("Active"))
        peer_apiurl = from_list(from_str, obj.get("PeerAPIURL"))
        in_network_map = from_bool(obj.get("InNetworkMap"))
        in_magic_sock = from_bool(obj.get("InMagicSock"))
        in_engine = from_bool(obj.get("InEngine"))
        tags = from_union([lambda x: from_list(Tag, x), from_none], obj.get("Tags"))
        addrs = from_union(
            [from_none, lambda x: from_list(from_str, x)], obj.get("Addrs")
        )
        ssh_host_keys = from_union(
            [from_none, lambda x: from_list(from_str, x)], obj.get("sshHostKeys")
        )
        primary_routes = from_union(
            [from_none, lambda x: from_list(from_str, x)], obj.get("PrimaryRoutes")
        )
        key_expiry = from_union([from_datetime, from_none], obj.get("KeyExpiry"))
        capabilities = from_union(
            [lambda x: from_list(from_str, x), from_none], obj.get("Capabilities")
        )
        return Self(
            id,
            public_key,
            host_name,
            dns_name,
            os,
            user_id,
            tailscale_ips,
            cur_addr,
            relay,
            rx_bytes,
            tx_bytes,
            created,
            last_write,
            last_seen,
            last_handshake,
            online,
            exit_node,
            exit_node_option,
            active,
            peer_apiurl,
            in_network_map,
            in_magic_sock,
            in_engine,
            tags,
            addrs,
            ssh_host_keys,
            primary_routes,
            key_expiry,
            capabilities,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["ID"] = from_str(self.id)
        result["PublicKey"] = from_str(self.public_key)
        result["HostName"] = from_str(self.host_name)
        result["DNSName"] = from_str(self.dns_name)
        result["OS"] = to_enum(OS, self.os)
        result["UserID"] = to_float(self.user_id)
        result["TailscaleIPs"] = from_list(from_str, self.tailscale_ips)
        result["CurAddr"] = from_str(self.cur_addr)
        result["Relay"] = to_enum(Relay, self.relay)
        result["RxBytes"] = from_int(self.rx_bytes)
        result["TxBytes"] = from_int(self.tx_bytes)
        result["Created"] = self.created.isoformat()
        result["LastWrite"] = self.last_write.isoformat()
        result["LastSeen"] = self.last_seen.isoformat()
        result["LastHandshake"] = self.last_handshake.isoformat()
        result["Online"] = from_bool(self.online)
        result["ExitNode"] = from_bool(self.exit_node)
        result["ExitNodeOption"] = from_bool(self.exit_node_option)
        result["Active"] = from_bool(self.active)
        result["PeerAPIURL"] = from_list(from_str, self.peer_apiurl)
        result["InNetworkMap"] = from_bool(self.in_network_map)
        result["InMagicSock"] = from_bool(self.in_magic_sock)
        result["InEngine"] = from_bool(self.in_engine)
        if self.tags is not None:
            result["Tags"] = from_union(
                [lambda x: from_list(lambda x: to_enum(Tag, x), x), from_none],
                self.tags,
            )
        result["Addrs"] = from_union(
            [from_none, lambda x: from_list(from_str, x)], self.addrs
        )
        if self.ssh_host_keys is not None:
            result["sshHostKeys"] = from_union(
                [from_none, lambda x: from_list(from_str, x)], self.ssh_host_keys
            )
        if self.primary_routes is not None:
            result["PrimaryRoutes"] = from_union(
                [from_none, lambda x: from_list(from_str, x)], self.primary_routes
            )
        if self.key_expiry is not None:
            result["KeyExpiry"] = from_union(
                [lambda x: x.isoformat(), from_none], self.key_expiry
            )
        if self.capabilities is not None:
            result["Capabilities"] = from_union(
                [lambda x: from_list(from_str, x), from_none], self.capabilities
            )
        return result


@dataclass
class User:
    id: float
    login_name: str
    display_name: str
    profile_pic_url: str
    roles: List[Any]

    @staticmethod
    def from_dict(obj: Any) -> "User":
        assert isinstance(obj, dict)
        id = from_float(obj.get("ID"))
        login_name = from_str(obj.get("LoginName"))
        display_name = from_str(obj.get("DisplayName"))
        profile_pic_url = from_str(obj.get("ProfilePicURL"))
        roles = from_list(lambda x: x, obj.get("Roles"))
        return User(id, login_name, display_name, profile_pic_url, roles)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ID"] = to_float(self.id)
        result["LoginName"] = from_str(self.login_name)
        result["DisplayName"] = from_str(self.display_name)
        result["ProfilePicURL"] = from_str(self.profile_pic_url)
        result["Roles"] = from_list(lambda x: x, self.roles)
        return result


@dataclass
class TailscaleData:
    version: str
    tun: bool
    backend_state: str
    auth_url: str
    tailscale_ips: List[str]
    tailscale_data_self: Self
    health: List[str]
    magic_dns_suffix: str
    current_tailnet: CurrentTailnet
    cert_domains: List[str]
    peer: Dict[str, Self]
    user: Dict[str, User]

    @staticmethod
    def from_dict(obj: Any) -> "TailscaleData":
        assert isinstance(obj, dict)
        version = from_str(obj.get("Version"))
        tun = from_bool(obj.get("TUN"))
        backend_state = from_str(obj.get("BackendState"))
        auth_url = from_str(obj.get("AuthURL"))
        tailscale_ips = from_list(from_str, obj.get("TailscaleIPs"))
        tailscale_data_self = Self.from_dict(obj.get("Self"))
        health = from_list(from_str, obj.get("Health"))
        magic_dns_suffix = from_str(obj.get("MagicDNSSuffix"))
        current_tailnet = CurrentTailnet.from_dict(obj.get("CurrentTailnet"))
        cert_domains = from_list(from_str, obj.get("CertDomains"))
        peer = from_dict(Self.from_dict, obj.get("Peer"))
        user = from_dict(User.from_dict, obj.get("User"))
        return TailscaleData(
            version,
            tun,
            backend_state,
            auth_url,
            tailscale_ips,
            tailscale_data_self,
            health,
            magic_dns_suffix,
            current_tailnet,
            cert_domains,
            peer,
            user,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["Version"] = from_str(self.version)
        result["TUN"] = from_bool(self.tun)
        result["BackendState"] = from_str(self.backend_state)
        result["AuthURL"] = from_str(self.auth_url)
        result["TailscaleIPs"] = from_list(from_str, self.tailscale_ips)
        result["Self"] = to_class(Self, self.tailscale_data_self)
        result["Health"] = from_list(from_str, self.health)
        result["MagicDNSSuffix"] = from_str(self.magic_dns_suffix)
        result["CurrentTailnet"] = to_class(CurrentTailnet, self.current_tailnet)
        result["CertDomains"] = from_list(from_str, self.cert_domains)
        result["Peer"] = from_dict(lambda x: to_class(Self, x), self.peer)
        result["User"] = from_dict(lambda x: to_class(User, x), self.user)
        return result


def tailscale_data_from_dict(s: Any) -> TailscaleData:
    return TailscaleData.from_dict(s)


def tailscale_data_to_dict(x: TailscaleData) -> Any:
    return to_class(TailscaleData, x)
