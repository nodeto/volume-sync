import json
import subprocess
from typing import List

from volume_sync.models.docker_volume import docker_volume_from_dict
from volume_sync.models.tailscale import Tag, TailscaleData, tailscale_data_from_dict


def complete_host_and_volume(incomplete: str) -> List[str]:
    """
    Given an incomplete host name, return a list of valid host names.

    :param incomplete: The incomplete host name.
    :return: A list of valid host names.
    """
    if "/" in incomplete:
        host, incomplete_volume_name = incomplete.split("/")
        try:
            volume_names: List[str] = [
                docker_volume_from_dict(json.loads(x)).name
                for x in subprocess.check_output(
                    [
                        "ssh",
                        f"root@{host}",
                        "docker",
                        "volume",
                        "ls",
                        "--format",
                        "json",
                    ],
                    text=True,
                    timeout=3,
                ).splitlines()
            ]
        except subprocess.CalledProcessError:
            return []
        return [
            f"{host}/{x}" for x in volume_names if x.startswith(incomplete_volume_name)
        ]
    else:
        tailscale_data: TailscaleData = tailscale_data_from_dict(
            json.loads(
                subprocess.check_output(
                    [
                        "/Applications/Tailscale.app/Contents/MacOS/Tailscale",
                        "status",
                        "--json",
                    ],
                    text=True,
                )
            )
        )
        options = [
            f"{x.host_name}/"
            for x in tailscale_data.peer.values()
            if x.host_name.startswith(incomplete)
            and x.tags is not None
            and Tag.TAG_SERVER in x.tags
        ]
        if len(options) == 1:
            return complete_host_and_volume(options[0])
        else:
            return options
