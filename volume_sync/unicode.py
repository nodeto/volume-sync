def print_unicode_colons() -> None:
    """
    Function to print various Unicode characters that can be used as substitutes for a colon.
    """
    # Define a list of Unicode characters that can substitute for a colon
    unicode_colons = ["\uFF1A", "\u02D0", "\u02F8", "\uA789", "\u2236"]

    # Iterate through the list and print each character
    for char in unicode_colons:
        print(f"The Unicode character for the substitute colon is: {char}")


# Run a simple doctest
if __name__ == "__main__":
    print_unicode_colons()
